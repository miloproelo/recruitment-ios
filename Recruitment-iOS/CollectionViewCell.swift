//
//  CollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 29/01/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import UIKit

final class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
}
