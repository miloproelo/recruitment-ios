//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 29/01/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import UIKit

final class CollectionViewController: UICollectionViewController {

    lazy var itemModels = [ItemModel]()

    override func viewDidLoad() {
        super.viewDidLoad()


        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemModels.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as? CollectionViewCell else { return UICollectionViewCell() }
        let itemModel = itemModels[indexPath.row]
        cell.backgroundColor = itemModel.color
        cell.nameLabel.text = itemModel.name
        return cell
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {

    var viewInset: UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
    }

    var interitemSpacing: CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let numberOfColumns: CGFloat = 2
        //this is due (insetForSectionAt:) is called after this method
        let actuallViewWidth = collectionView.bounds.size.width - viewInset.left - viewInset.right
        let cellWidth = (actuallViewWidth - interitemSpacing * numberOfColumns) / numberOfColumns

        return CGSize(width: cellWidth, height: cellWidth)

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return viewInset
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}

extension CollectionViewController: NetworkingManagerDelegate {

    func downloadedItems(_ items: [ItemModel]?, error: Error?) {
        if let items = items {
            itemModels.append(contentsOf: items)
            collectionView?.reloadData()
        } else {
            print(error?.localizedDescription ?? "Undefined error")
        }
    }
}
