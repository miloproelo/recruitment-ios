//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

final class DetailsViewController: UIViewController {

    fileprivate var itemModel: ItemModelKind?

    @IBOutlet weak var textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let itemModel = itemModel else { return }

        let title = (itemModel.name.characters.enumerated().map {index, character in
            return index % 2 == 0 ? String(character).lowercased() : String(character).uppercased()
        }).joined()

        self.title = title
        self.view.backgroundColor = itemModel.color

        NetworkingManager.sharedManager.downloadItemWithID(itemModel.id) { [weak self] (getResults: () throws -> ItemDetailsModel) in
            do {
                let itemDetails = try getResults()
                self?.textView.text = itemDetails.desc
            } catch let error {
                let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default)
                alertController.addAction(action)
                self?.present(alertController, animated: true, completion: nil)
            }
        }
    }

    func setupViewController(itemModel: ItemModelKind) {
        self.itemModel = itemModel
    }
    
}
