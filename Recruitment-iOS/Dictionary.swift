//
//  Dictionary.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 04/02/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import Foundation

extension Dictionary {

    fileprivate init(_ pairs: [Element]) {
        self.init()
        pairs.forEach { self[$0] = $1 }
    }

    func map<Key: Hashable, Value>( transform: (Element) throws -> (Key, Value)) rethrows -> [Key: Value] {
        return Dictionary<Key, Value>(try map(transform))
    }
}
