//
//  Error.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 30/01/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import Foundation

enum NetworkingManagerError: Error {
    case missingData
}

enum JSONDeserializationError: Error {
    case unexpectedValueType(actual: JSONValueType, expected: JSONValueType)
    case unsupportedObjectType(type: Any.Type)
    case missingKey(key: String)
}

extension JSONDeserializationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .unexpectedValueType(actual: let actual, expected: let expected):
            return NSLocalizedString("Expected " + String(describing: expected) + " got " + String(describing: actual), comment: "")
        case .unsupportedObjectType(type: let type):
            return NSLocalizedString("Got unexpected value of type " + String(describing: type), comment: "")
        case .missingKey(key: let missingKey):
            return NSLocalizedString("Key: " + missingKey + " not found", comment: "")
        }
    }
}
