//
//  ItemDetailsModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

struct ItemDetailsModel: ItemModelKind {

    let id: String
    let name: String
    let color: UIColor
    let desc: String
    
}
