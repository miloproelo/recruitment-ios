//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

protocol ItemModelKind {
    var id: String { get }
    var name: String { get }
    var color: UIColor { get }
}

struct ItemModel: ItemModelKind {

    let id: String
    let name: String
    let preview: String
    let color: UIColor
    
}
