//
//  JSONValue.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 30/01/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import Foundation

enum JSONValue {
    case number(NSNumber)
    case string(String)
    case array(Array<JSONValue>)
    case dictionary(Dictionary<String, JSONValue>)
}

enum JSONValueType {
    case number, string, array, dictionary
}

extension JSONValue {
    var type: JSONValueType {
        switch self {
        case .number: return .number
        case .string: return .string
        case .array: return .array
        case .dictionary: return .dictionary
        }
    }
}

extension JSONValue {

    init(_ object: AnyObject) throws {
        switch object {
        case let number as NSNumber:
            self = .number(number)
        case let string as String:
            self = .string(string)
        case let array as Array<AnyObject>:
            self = .array(try array.map { try JSONValue($0) })
        case let dictionary as Dictionary<String, AnyObject>:
            self = .dictionary(try dictionary.map { ($0, try JSONValue($1)) })

        default :
            throw JSONDeserializationError.unsupportedObjectType(type: type(of: object))
        }
    }

}

extension JSONValue {

    func getArray() throws -> [JSONValue] {
        switch self {
        case .array(let array):
            return array
        default:
            throw JSONDeserializationError.unexpectedValueType(actual: self.type, expected: JSONValueType.array)
        }
    }

    func getDictionary() throws -> [String: JSONValue] {
        switch self {
        case .dictionary(let dictionary):
            return dictionary
        default:
            throw JSONDeserializationError.unexpectedValueType(actual: self.type, expected: JSONValueType.dictionary)
        }
    }

    func getNumber() throws -> NSNumber {
        switch self {
        case .number(let number):
            return number
        default:
            throw JSONDeserializationError.unexpectedValueType(actual: self.type, expected: JSONValueType.number)
        }
    }

    func getString() throws -> String {
        switch self {
        case .string(let string):
            return string
        default:
            throw JSONDeserializationError.unexpectedValueType(actual: self.type, expected: JSONValueType.string)
        }
    }

    func get(_ key: String) throws -> JSONValue {
        guard case .dictionary(let dictionary) = self else {
            throw JSONDeserializationError.unexpectedValueType(actual: self.type, expected: JSONValueType.dictionary)
        }
        guard let value = dictionary[key] else {
            throw JSONDeserializationError.missingKey(key: key)
        }
        return value
    }
}
