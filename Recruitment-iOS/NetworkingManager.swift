//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

// I think that completion blocks are a better way of solving network calls
// but leaving this one to show that I solved the issue with the retain cycle of the delegate.
protocol NetworkingManagerDelegate: class {
    
    func downloadedItems(_ items: [ItemModel]?, error: Error?)
}

final class NetworkingManager: NSObject {

    static var sharedManager = NetworkingManager()
    
    weak var delegate:NetworkingManagerDelegate?
    
    func downloadItems() {
        request(filename: "Items.json") { dictionary in
            do {
                let data = try JSONValue(dictionary as AnyObject).get("data")
                let array = try data.getArray()
                let result = try array.map { item throws -> ItemModel in
                    let identifer = try item.get("id").getString()
                    let attributes = try item.get("attributes")
                    let name = try attributes.get("name").getString()
                    let preview = try attributes.get("preview").getString()
                    let colorString = try attributes.get("color").getString()
                    let color = UIColor.colorWithString(stringValue: colorString)
                    return ItemModel(id: identifer, name: name, preview: preview, color: color)
                }
                self.delegate?.downloadedItems(result, error: nil)
            } catch let error {
                self.delegate?.downloadedItems(nil, error: error)
            }
        }
    }
    
    func downloadItemWithID(_ id: String, completionBlock:@escaping (_ getResults:() throws -> ItemDetailsModel) -> Void) {
        let filename = "Item\(id).json"
        request(filename: filename) { dictionary in
            do {
                let data = try JSONValue(dictionary as AnyObject).get("data")
                let identifer = try data.get("id").getString()
                let attributes = try data.get("attributes")
                let name =  try attributes.get("name").getString()
                let colorString = try attributes.get("color").getString()
                let color = UIColor.colorWithString(stringValue: colorString)
                let desc = try attributes.get("desc").getString()
                let itemModelDetails = ItemDetailsModel(id: identifer, name: name, color: color, desc: desc)
                completionBlock { itemModelDetails }
            } catch let error {
                completionBlock { throw error }
            }
        }
    }

    private func request(filename:String, completionBlock:@escaping (Dictionary<String, AnyObject>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let dictionary = JSONParser.jsonFromFilename(filename) {
                completionBlock(dictionary)
            } else {
                completionBlock([:])
            }
        }
    }
    
}
