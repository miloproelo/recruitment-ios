//
//  TableViewCell.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 29/01/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import UIKit

final class TableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var previewLabel: UILabel!
}
