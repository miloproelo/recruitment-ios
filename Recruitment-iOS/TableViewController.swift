//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

final class TableViewController: UITableViewController {

    lazy var itemModels = [ItemModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModels.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as? TableViewCell else { return UITableViewCell() }
        let itemModel = itemModels[indexPath.row]
        cell.backgroundColor = itemModel.color
        cell.titleLabel.text = itemModel.name
        cell.previewLabel.text = itemModel.preview
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsViewController  = segue.destination as? DetailsViewController {
            let selectedIndex = tableView.indexPathForSelectedRow?.row ?? 0
            detailsViewController.setupViewController(itemModel: itemModels[selectedIndex])
        }
    }
}

extension TableViewController: NetworkingManagerDelegate {

    func downloadedItems(_ items: [ItemModel]?, error: Error?) {
        if let items = items {
            itemModels = items
            tableView.reloadData()
        } else {
            let errorString = error?.localizedDescription ?? "Undefined error"
            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
