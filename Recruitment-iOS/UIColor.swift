//
//  UIColor.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 29/01/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UIColor {

    /// Converts given string into UIColor represenation.
    /// - Important: Supports only following colors:
    /// Red, Blue, Green, Yellow, Purple. 
    /// Default is black.
    ///
    /// - Parameter stringValue: String to convert.
    /// - Returns: UIColor representation.
    static func colorWithString(stringValue: String) -> UIColor {
        switch stringValue {
        case "Red":
            return UIColor.red
        case "Blue":
            return UIColor.blue
        case "Green":
            return UIColor.green
        case "Yellow":
            return UIColor.yellow
        case "Purple":
            return UIColor.purple
        default:
            return UIColor.black
        }
    }
}
