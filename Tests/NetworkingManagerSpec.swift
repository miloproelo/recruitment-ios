//
//  Tests.swift
//  Tests
//
//  Created by Miłosz Filimowski on 30/01/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

final class NetworkingManagerDelegateMock: NetworkingManagerDelegate {

    var asyncExpectation: XCTestExpectation?
    var result: [ItemModel]?

    func downloadedItems(_ items: [ItemModel]?, error: Error?) {
        guard let expectation = asyncExpectation else {
            XCTFail("Wrong delegate mockup setup")
            return
        }
        result = items
        expectation.fulfill()
    }
}

final class NetworkingManagerTest: XCTestCase {

    var sut: NetworkingManager!
    var delegateMock: NetworkingManagerDelegateMock!

    override func setUp() {
        super.setUp()
        sut = NetworkingManager()
        delegateMock = NetworkingManagerDelegateMock()
    }

    override func tearDown() {
        super.tearDown()
        sut = nil
        delegateMock = nil
    }

    func testPerformingDownloadItemsRequestOnDelegate() {
        sut.delegate = delegateMock

        delegateMock.asyncExpectation = expectation(description: "Should call delegate")

        sut.downloadItems()

        waitForExpectations(timeout: 5) { [unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertTrue(self.delegateMock.result != nil)
        }
    }
}
