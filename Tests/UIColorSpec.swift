//
//  UIColorSpec.swift
//  Recruitment-iOS
//
//  Created by Miłosz Filimowski on 05/02/17.
//  Copyright © 2017 Untitled Kingdom. All rights reserved.
//

import Nimble
import Quick

class UIColorSpec: QuickSpec {
    override func spec() {
        describe("UIColor colorWithString method") {
            context("when valid string is passed", {
                it("should change it to proper UIColor representation", closure: {
                    expect(UIColor.colorWithString(stringValue: "Red")).to(equal(UIColor.red))
                    expect(UIColor.colorWithString(stringValue: "Blue")).to(equal(UIColor.blue))
                    expect(UIColor.colorWithString(stringValue: "Green")).to(equal(UIColor.green))
                    expect(UIColor.colorWithString(stringValue: "Yellow")).to(equal(UIColor.yellow))
                    expect(UIColor.colorWithString(stringValue: "Purple")).to(equal(UIColor.purple))
                })
            })
            context("when not valid string is passed", { 
                it("should return default value", closure: { 
                    expect(UIColor.colorWithString(stringValue: "abcd")).to(equal(UIColor.black))
                    expect(UIColor.colorWithString(stringValue: "")).to(equal(UIColor.black))
                })
            })
        }
    }
}
